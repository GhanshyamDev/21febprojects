<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\AdminController;
use App\Http\Controllers\SubAdmin\OemAdminController;
use App\Http\Controllers\TicketController;
use App\Http\Controllers\Notification;
use App\Http\Controllers\HomeController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
Route::get('/', function () {
   return redirect(route('login'));
});
Route::get('/home',  [OemAdminController::class, 'index'])->name('home');
Auth::routes();
Route::group(['middleware' => 'role:oem'], function() {
Route::get('oemAdmin/userCreate', [OemAdminController::class, 'CreateUser']);
Route::post('oemAdmin/userCreate', [OemAdminController::class, 'CreateUser'])->name('oemAdmin.userCreate');
Route::get('oemAdmin/userEdit/{id}', [OemAdminController::class, 'EditUser']);
Route::post('oemAdmin/updateUser', [OemAdminController::class, 'updateUser']);
Route::get('oemAdmin/deleteUser/{id}', [OemAdminController::class, 'DeleteUser']);

});
//send notification 
Route::get('oemAdmin/createCategory', [OemAdminController::class, 'createCategory']);
Route::post('createCategory', [OemAdminController::class, 'createCategory'])->name('oemAdmin.createCategory');
Route::get('oemAdmin/categoryEdit/{id}', [OemAdminController::class, 'categoryEdit']); 
Route::post('categoryUpdate', [OemAdminController::class, 'categoryUpdate'])->name('oemAdmin.categoryUpdate'); 
Route::get('oemAdmin/categoryDelete/{id}', [OemAdminController::class, 'categoryDel']); 

Route::get('oemAdmin/createGeography', [OemAdminController::class, 'createGeography']);
Route::post('createGeography', [OemAdminController::class, 'createGeography'])->name('oemAdmin.createGeography');
Route::get('oemAdmin/geographyEdit/{id}', [OemAdminController::class, 'geographyEdit']); 
Route::post('geographyUpdate', [OemAdminController::class, 'geographyUpdate'])->name('oemAdmin.geographyUpdate'); 
 Route::get('oemAdmin/geographyDelete/{id}', [OemAdminController::class, 'geographyDel']); 
//Renew AMC 

Route::get('oemAdmin/ViewRenewAmc',  [OemAdminController::class, 'viewRenew_amcList']);
Route::post('select-serviceType',  [OemAdminController::class, 'selectServiceType'])->name('select-serviceType');
Route::get('oemAdmin/ViewServiceRequest',  [OemAdminController::class, 'viewService_request']);
Route::post('oemAdmin/AjaxServiceRequest',  [OemAdminController::class, 'AjaxServiceRequest']);
Route::post('oemAdmin/markServiceRequest',  [OemAdminController::class, 'AjaxServiceRequest_read']);
Route::get('oemAdmin/report',  [OemAdminController::class, 'report']);
Route::get('oemAdmin/my-account', [OemAdminController::class, 'myAccount']);
Route::post('my-account', [OemAdminController::class, 'updateAccount'])->name('oemAdmin.my-account');


Route::post('sendServiceReqAll', [Notification::class, 'sendServiceNotification'])->name('sendServiceReqAll');
Route::get('userNotification', [Notification::class, 'sendUserNotification']);
Route::post('userNotification', [Notification::class, 'sendUserNotification'])->name('userNotification');
Route::post('resendNotification', [Notification::class, 'resendUserNotification'])->name('resendNotification');


// Ticket ,  Notifiacation
Route::get('oemAdmin/viewTicket', [TicketController::class, 'Oemticket']);
Route::post('approveUser', [OemAdminController::class, 'ApproveUser']);
Route::post('opencloseTicket', [TicketController::class, 'openCloseTicket']);
Route::post('replyTicket', [TicketController::class, 'replyTicket'])->name('replyTicket');


 
 // admin 

Route::get('admin/dashboard', [AdminController::class, 'index'])->name('admin.route')->middleware('admin');
Route::get('admin/ticket', [TicketController::class, 'Adminticket'])->middleware('admin');


Route::post('sendMarkTicket', [TicketController::class, 'markTicket'])->name('sendMarkTicket');
Route::get('viewTicket',  [TicketController::class, 'viewTicket']);


 