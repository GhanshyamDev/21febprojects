@extends('layouts.customer')

@section('content')
<div class="content-wrapper">
  
    <div class="page-header" style="margin: 1rem 0 0.1rem 0;">
        <h2 class="page-title ml-4" style="font-size:25px;">
            Welcome <?php echo session('Customer_logged')['name'];?>
        </h2>
        <nav aria-label="breadcrumb">
          <ul class="breadcrumb">
            <a  class="  btn-icon-text btn-sm" href="#"><i class="fa fa-download" aria-hidden="true" style="font-size: 15px;"></i> Terms & Condition</a> 
             <a  class="  btn-sm" href="#"><i class="fa fa-download"  aria-hidden="true" style="font-size: 15px;"></i> Invoice</a> 
             <a href="{{ url('users/pdf') }}/{{ $userProductDetails['product_id'] }}" class="  btn-sm" href="#"><i class="fa fa-print"  aria-hidden="true" style="font-size: 15px;"></i>Print</a> 
          </ul>
        </nav> 
      </div>
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid"  id="pageDIv">
          <div class="row">
           <div class="col-md-12">
             <div class="card">
                  <h2></h2>
                <div class="card-body">
                     <h4 class="card-title">User Details</h4>
                 <form method="post"  >
                    <div class="form-row">
                       <div class="col-md-4">
                            <div class="form-group">
                                <label >Name</label>
                                <input class="form-control" name="name" type="text" value="{{ session('Customer_logged')['name'] }}">
                             </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label >Email</label>
                                 <input class="form-control" name="email" type="email" value="{{ session('Customer_logged')['email'] }}">
                              </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label >Mobile</label>
                                 <input class="form-control" maxlength="10" name="phone" type="text" value="{{ $userProductDetails['mobile'] }}">
                               </div> 
                        </div>
                     </div>
                     <div class="form-row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label >Aadhar</label>
                                 <input class="form-control  " maxlength="10" name="Aadhar" type="text" value="XXXXXXXX">
                              </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label >Location</label>
                                 <input class="form-control"   name="location" type="text" value="{{ $city['city_name'] }}, {{ $state['state_name'] }}, {{ $userProductDetails['pincode'] }}">
                               </div> 
                        </div>
                     </div>
                     
                </form>
                </div>
             </div>
            </div>
          </div> 
          <br/>
          
           <div class="row">
           <div class="col-md-12">
             <div class="card">
                  <h2></h2>
                <div class="card-body">
                     <h4 class="card-title">Product Details</h4>
                 <form method="post"  >
                    <div class="form-row">
                      <div class="col-md-4">
                            <div class="form-group">
                                <label>Product Id</label>
                                <input class="form-control" name="product_id" type="text" value="0000{{ $userProductDetails['product_id'] }}">
                             </div>
                        </div>
                       <div class="col-md-4">
                            <div class="form-group">
                                <label>Product Name</label>
                                <input class="form-control" name="name" type="text" value="{{ $userProductDetails['product_name'] }}">
                             </div>
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Price</label>
                                 <input class="form-control" name="price" type="text" value="{{ $userProductDetails['product_price'] }}">
                              </div>
                        </div>
                       
                     </div>
                     <div class="form-row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Manufecturer</label>
                                 <input class="form-control" maxlength="10" name="phone" type="text" value="{{ $userProductDetails['category'] }}">
                               </div> 
                        </div>
                        <div class="col-md-4">
                            <div class="form-group">
                              <?php
                           
                                 $date1 = \Carbon\Carbon::parse($userProductDetails['warranty_start']);
                                  $date2 =\Carbon\Carbon::parse($userProductDetails['warranty_end']);
                                    
                              ?>
                                <label>Warranty Status</label>
                                 <input class="form-control  " maxlength="10" name="Aadhar" type="text" value="{{ date('d M Y', strtotime($userProductDetails['pauchase_date']))  }}(<?php echo $date1->diffInDays($date2); ?> Days)">
                              </div>
                         </div>
                       <div class="col-md-4">
                            <div class="form-group">
                                <label>Warranty </label>
                                 <input class="form-control " name="warranty" type="text" value="{{ date('d M Y', strtotime($userProductDetails['warranty_start'])) }}- To -{{ date('d M Y', strtotime($userProductDetails['warranty_end']))  }}">
                              </div> 
                         </div>
                          
                      </div>
                    
                    
                </form>
                </div>
             </div>
            </div>
          </div>
         <br/>
      
       <div class="row mt-3">
         <div class="col-lg-3 col-sm-6 col-md-5 grid-margin  ">
             <div class="card">
            <div class="card-body text-center">
              <h4 class="card-title" style=" margin-left: 40%;"> <img src="{{ asset('img/service.png')}}" style="width:40px;"></h4>
              <p class="card-description"><a class="text-primary f1  " href="tel:123-456-7890">Call Service Center</a></code></p>
             </div>
          </div> 
        </div>
         <div class="col-lg-3 col-sm-6 col-md-5 grid-margin  ">
          <div class="card">
            <div class="card-body text-center">
              <h4 class="card-title" style=" margin-left: 40%;"> <img src="{{ asset('img/renew.webp')}}" style="width:40px;"></h4>
              <p class="card-description"><a href="{{ url('users/ticketCreate') }}/{{ Crypt::encryptString($userProductDetails['product_id']) }}" class="text-primary f1  " href="#">Renew Service</a></code></p>
             </div>
          </div>
        </div>

        <div class="col-lg-3 col-sm-4 col-md-5 grid-margin  ">
          <div class="card">
            <div class="card-body text-center">
              <h4 class="card-title" style=" margin-left: 40%;"> <img src="{{ asset('img/ser-req.png')}}" style="width:40px;"></h4>
               <p class="card-description"><a href="{{ url('users/CreateService') }}/{{ Crypt::encryptString($userProductDetails['product_id']) }}" class="text-primary f1  " href="#">Create Service Request</a></code></p>
              <p class="card-description text-primary f1"> </p>
             </div>
          </div>
        </div> 

        <div class="col-lg-3 col-sm-4  col-md-5 grid-margin  ">
          <div class="card">
            <div class="card-body text-center">
              <h4 class="card-title" style=" margin-left: 40%;"> <img src="{{ asset('img/whatsapp.png')}}" style="width:40px;"></h4>
              <p class="card-description text-primary f1">WhatsApp</p>
             </div>
          </div>
        </div> 
      </div>
     </div> 
</section>
 
</div>
@endsection
