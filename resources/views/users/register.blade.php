<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="icon" type="image/icon" sizes="32x32" href="{{ asset('img/favicon.png') }}">
  <!-- CSRF Token -->
   <meta name="csrf-token" content="{{ csrf_token() }}"> 

  <title>{{ __('Registration') }}</title>

  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
  <link rel="stylesheet" href="{{ asset('assets/fontawesome-free/css/all.min.css') }}">
   <link rel="stylesheet" href=" {{ asset('assets/css/style.css') }}">
<!--   <link rel="stylesheet" href=" {{ asset('assets/css/login.css') }}"> -->
  <!-- Scripts --> 
  <!--   <script src="{{ asset('js/app.js') }}" defer></script> -->

  <!-- Fonts -->
  <link rel="dns-prefetch" href="//fonts.gstatic.com">
  <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
  <style>
    .overlay{
      display: none;
      position: fixed;
      width: 100%;
      height: 100%;
      top: 0;
      left: 0;
      z-index: 999;
      background: rgba(255,255,255,0.8) url({{ asset("img/loading.gif") }}) center no-repeat;
}
body{
  text-align: center;
}
/* Turn off scrollbar when body element has the loading class */
body.loading{
  overflow: hidden;   
}
/* Make spinner image visible when body element has the loading class */
body.loading .overlay{
  display: block;
}
</style>
<!-- Styles -->
<!--  <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
</head>

<body >
  <div class="container-scroller">
      <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="content-wrapper d-flex align-items-center auth">
          <div class="row flex-grow">
            <div class="col-lg-4 mx-auto">
              <div class="auth-form-light text-left p-2">
                <div class="brand-logo text-center">
                  <img src="{{ asset('img/clyfe.png') }}">
                </div>
                @if(Session::has('failed'))
                   <h4 style="color:#d32535;"> {{Session::get('failed')}}</h4>
                 @endif
                  @if(!empty($error))
                   <h4 style="color:#d32535;"> {{ $error }}</h4>
                 @endif
                 <div class="card-body registerDiv">
         
                  <form class="form-group ">
                    @csrf
                    <div class="form-group">
                      <input type="text" class="form-control @error('name') is-invalid @enderror"   id="name" name="name" value="{{ old('name') }}"   autocomplete="name" placeholder="Enter Name">
                      <span class="text-danger " style="text-align: left;" id="nameError">  <strong > </strong> </span>

                    </div>
                    <div class="form-group">
                      <input type="email" class="form-control @error('email') is-invalid @enderror"  id="email"    name="email" value="{{ old('email') }}"   autocomplete="email" placeholder="Enter Email">
                      <span class="text-danger " id="EmailError">   </span>

                    </div>
                    <div class="form-group">
                      <input type="text" class="form-control @error('mobile') is-invalid @enderror" maxlength="10"  id="mobile" name="mobile" value="{{ old('mobile') }}"   autocomplete="mobile" placeholder="Enter Mobile">
                      <span class="text-danger " id="mobileError">    </span>

                    </div>
                    <div class="form-group">
                      <input type="password" class="form-control @error('password') is-invalid @enderror"   id="password" name="password" value="{{ old('password') }}"   autocomplete="password" placeholder="Enter Password">

                      <span class="text-danger "  id="passwordError">  <strong id="passwordError"> </strong> </span>

                    </div>

                    <div class="form-group">

                      <select class="form-control select2 @error('state') is-invalid @enderror stateClss" id="state" name="state" onchange="getStateCity()" style="width: 100%;"  >
                        <option value=" ">Select State</option>
                        @if (count($states))
                        @foreach ($states as $state)  
                        <option value="{{ $state['state_id']}}">{{ $state['state_name']}}</option>
                        @endforeach
                        @else
                        <option>State Not Found</option> 
                        @endif
                      </select>

                      <span class="text-danger " id="stateError">  </span>

                    </div>
                    <div class="form-group">
                      <select type="text" class="form-control @error('city') is-invalid @enderror cityClass"  id="city" name="city" value="{{ old('city') }}"     placeholder="Enter City">
                        <option value=" ">Select City</option>
                      </select>
                      @error('city')
                      <span  class="text-danger "  id="cityError">  <strong>{ </strong> </span>
                      @enderror
                    </div>
                    <div class="form-group">
                      <input type="number" class="form-control @error('pincode') is-invalid @enderror"   id="pincode" name="pincode" value="{{ old('pincode') }}"   autocomplete="pincode"  placeholder="Enter Pincode" pattern="[1-9]{1}[0-9]{9}">  
                      <span class="text-danger " id="pincodeError"> </span>

                    </div>
                      
                    <div class="social-auth-links text-center mt-2 mb-3">
                      <button type="submit" class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btnk" id="RegisterSubmit" >  {{ __('Register') }}  </button>
                    </div>
                  </form>
                   
                </div>
                 <div class="card-body" id="OtpDiv" style="display: none">
                    <form class="form-group">
                       @csrf
                        <div class="form-group">
                          <span class="text-success" id="OtpVerify"></span>
                          <input type="hidden" name="cust_id" id="userid" value="">
                          <input type="text" class="form-control" id="otp"  name="otp"  placeholder="Enter OTP">
                          <span class="text-danger" id="otpError"> </span>
                        </div>
                        <div class="form-group">
                          <button type="submit" class="btn btn-primary btn-block"  id="otpSubmit" style="width: 80px;"> Verify</button>
                        </div>
                    </form>
                 </div>
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
  <div class="overlay"></div>
  <script src=" {{ asset('assets/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('assets/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
  <script src=" {{ asset('dist/js/adminlte.js') }}"></script>
  <script type="text/javascript">
    function getStateCity(){ 
      var state= $(".stateClss").val();

      $.ajax({
        url: "{{ url('users/getCitys') }}",
        type:"POST",
        data:{
          "_token": "{{ csrf_token() }}",
          state:state, 
        },
        success:function(response){
          $.each(response.citys, function(key, value) {
            $(".cityClass").append($('<option></option>').attr('value', value.city_id).text(value.city_name));
          });

        },
      });
    };

    $(document).ready(function() {
      $("#RegisterSubmit").click(function(e){
        e.preventDefault();


        var _token = $("input[name='_token']").val();
        var name = $("#name").val();
        var email = $("#email").val();
        var mobile = $("#mobile").val();
        var password = $("#password").val();
        var state = $("#state").val();
        var city = $("#city").val();
        var pincode = $("#pincode").val();



        $.ajax({
          url: "{{ route('users.signUp') }}",
          type:'POST',
          data: {_token:_token, name:name, email:email, mobile:mobile, password:password, state:state, city:city, pincode:pincode},
          beforeSend: function(){
             $("body").addClass("loading"); 
          },
          success: function(response) {
            if(response.success == 200){
              $(".registerDiv").hide();
              $("#OtpDiv").show();

              $("#OtpVerify").text(response.mesg);
              $("#userid").val(response.userid);

            } else {

            }
          },
          error: function(response) {

            $("#nameError").text(response.responseJSON.errors.name);
            $('#EmailError').text(response.responseJSON.errors.email);
            $('#mobileError').text(response.responseJSON.errors.mobile);
            $('#passwordError').text(response.responseJSON.errors.password);
            $('#stateError').text(response.responseJSON.errors.state);
            $('#cityError').text(response.responseJSON.errors.city);
            $('#pincodeError').text(response.responseJSON.errors.pincode);
          },

        complete:function(response){

             $("body").removeClass("loading"); 
        },
     });


      }); 

      function printErrorMsg (msg) {
        $(".print-error-msg").find("ul").html('');
        $(".print-error-msg").css('display','block');
        $.each( msg, function( key, value ) {
          $(".print-error-msg").find("ul").append('<li>'+value+'</li>');
        });
      }
    });

  $("#otpSubmit").click(function(e){
      e.preventDefault();
      var _token = $("input[name='_token']").val();
   
      var otp = $("#otp").val();
      var userid = $("#userid").val();
      $("#OtpVerify").text(' ');

      $.ajax({
        url: "{{ route('users.postVerify') }}",
        type:'POST',
        data: {_token:_token,
                otp: otp , 
                cust_id:userid },
        beforeSend: function(){
          $("body").addClass("loading");
        },
        success: function(response) {
           if(response.success == 200) {
             
              $("#OtpVerify").text(response.mesg);
              window.setTimeout(function() {
                    window.location.href = 'users/dashboard';
              }, 5000);
                window.location.replace('{{ url("users/dashboard")}}');
            } else {
             $("#OtpVerify").text(response.mesg);
            }
        },
        complete:function(response){

          $("body").removeClass("loading"); 
        },
      })     
    }); 
  </script>
</body>
</html>
