<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
 
 <link rel="icon" type="image/png" sizes="32x32" href="{{ asset('img/favicon.png') }}">
    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ __('User Login') }}</title>
 
   <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&display=fallback">
   <link rel="stylesheet" href="{{ asset('assets/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/icheck-bootstrap/icheck-bootstrap.min.css') }}">
   <link rel="stylesheet" href=" {{ asset('assets/css/style.css') }}">
    <!-- Scripts -->
 
    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">
<style>
    .bs-example{
      margin: 20px;
        position: relative;
    }
</style>
<script>
    $(document).ready(function(){
        $(".show-toast").click(function(){
            $("#myToast").toast('show');
        });
    });
</script>
    <!-- Styles -->
   <!--  <link href="{{ asset('css/app.css') }}" rel="stylesheet"> -->
</head>
<body >
  <div class="container-scroller">
      <div class="container-fluid page-body-wrapper full-page-wrapper">
        <div class="content-wrapper d-flex align-items-center auth">
          <div class="row flex-grow">
            <div class="col-lg-4 mx-auto">
              <div class="auth-form-light text-left p-5">
                <div class="brand-logo text-center">
                  <img src="{{ asset('img/clyfe.png') }}">
                </div>
                @if(Session::has('failed'))
                   <h4 style="color:#d32535;"> {{Session::get('failed')}}</h4>
                 @endif
                  @if(!empty($error))
                   <h4 style="color:#d32535;"> {{ $error }}</h4>
                 @endif
                 <form method="POST" action="{{ url('users/authenticate') }}">
                     @csrf
                     <div class="form-group mb-1">
                      <label>Email</label>
                        <input type="email" class="form-control @error('email') is-invalid @enderror"   name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Enter Email">
                       
                       </div>
                        @error('email')
                          <span class="invalid-feedback" role="alert">
                              <strong>{{ $message }}</strong>
                          </span>
                          @enderror
                           
                        <div class="form-group pt-3 mb-1">
                           <label>Password</label>
                         <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Enter Password">
                          
                        </div>
                      @error('password')
                      <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                      </span>
                      @enderror

                     <div class="mt-3 mb-0 "><a href="{{ url('users/register')}}">Register</a></div>
                       <div class="text-center mt-4 font-weight-light"> 

                          <button type="submit" class="btn btn-block btn-primary btn-lg font-weight-medium auth-form-btnk" >  {{ __('Sign In') }}  </button>
                      </div>
                    </form>
 
              </div>
            </div>
          </div>
        </div>
        <!-- content-wrapper ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
  <div class="toast" id="myToast" style="position: absolute; top: 0; right: 0;">
    <div class="toast-header">
        <strong class="mr-auto"><i class="fa fa-grav"></i> We miss you!</strong>
        <small>11 mins ago</small>
        <button type="button" class="ml-2 mb-1 close" data-dismiss="toast">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
    <div class="toast-body">
        <div>It's been a long time since you visited us. We've something special for you. <a href="#">Click here!</a></div>
    </div>
</div>
<script src=" {{ asset('assets/jquery/jquery.min.js') }}"></script>
<script src="{{ asset('assets/bootstrap/js/bootstrap.bundle.min.js') }}"></script>
<script src=" {{ asset('dist/js/adminlte.js') }}"></script>
</body>
</html>
