 @extends('layouts.admin')
 

@section('content')
 <div class="content-wrapper"> 
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
           
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v1</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">

        <div class="table-responsive mailbox-messages">

                <table class="table table-hover table-striped">
                  <tbody>  
              
                             @foreach ($ticketDetails as $view)  
                               <tr>
                                  
                                  <td class="mailbox-star"><a href="#"><i class="fas fa-star text-warning"></i></a></td>
                                  <td class="mailbox-name"><a href="read-mail.html">{{ $view['name'] }}</a></td>
                                  <td class="mailbox-subject"><b>AdminLTE 3.0 Issue</b> - {{ $view['description'] }}
                                  </td>
                                   <td class="mailbox-date">{{ date('d M  ', strtotime($view['created_at']))  }}  </td>
                                </tr> 
                             @endforeach
                           
                         </tbody>
                  <tbody>
                    
                  
                  
                  </tbody>
                </table>
                <!-- /.table -->
              </div>
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection
