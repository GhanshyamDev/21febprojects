  @extends('layouts.app')
 

@section('content')
 <div class="content-wrapper"> 
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mt-3">
          <div class="col-sm-6">
            <h3 class="mt-2">Notifications</h3>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <button type="button" onclick="SendNotification_modal()" class="btn btn-primary btn-sm tex-right"> Send Motifications</button>
            </ol>
          </div>  
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
  <section class="content">
      <div class="container-fluid">
        
       <div class="card">
        
        <div class="card-body">
          <div class="table-responsive ">
              <table id="" class="table table-striped table-bordered example" style="width:100%">
              <thead style="background-color: #2c349c;color: #fff;">
                  <tr>
                     <th>product</th>
                      <th>date</th>
                   </tr>
              </thead>
              <tbody>
                <?php $i=0; foreach($notifications as $noti) { $i++;?>
                    <tr>
                       <?php if($noti['Send_product_user'] == 0) { ?>
                         <td><a href="#"  onclick="notificationTO_user('<?php echo $noti['noti_id']; ?>','<?php echo $noti['product_name']; ?>','<?php echo $noti['message']; ?>')" id="getdata"> For All users</a></td>
                        <?php } else { ?>
                      <td><a href="#"  onclick="notificationTO_user('<?php echo $noti['noti_id']; ?>','<?php echo $noti['product_name']; ?>','<?php echo $noti['message']; ?>')" id="getdata"><?php  echo  $noti['product_name']; ?></a></td>
                       <?php }  ?> 
                      <td><?php  echo  date('d M Y', strtotime($noti['created_at'])) ; ?></td>
                    </tr>
                <?php } ?>
              </tbody>
              </table>
          </div>
           
        </div>
        <!-- /.row (main row) -->
       </div><!-- /.container-fluid -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection
<div class="modal fade" id="AdminNotification_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Notification</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <form method="post" action="{{ route('resendNotification') }}">
        {{ csrf_field() }}
          <div class="form-group">
            <label >Category:</label>
            <input type="hidden" class="form-control"  name="id" id="Adminid">
            <input type="text" class="form-control"  name="category" id="Adminproduct">
          </div>
          <div class="form-group">
            <label >Message:</label>
            <textarea class="form-control"  name="message" id="Adminmessage"  maxlength="100"  rows="6" cols="80"></textarea>
          </div>
          <div class="form-row">
              <input type="submit" class="btn btn-primary btn-block" name="submit" value="Resend"> 
              
            </div>
        </form>
      </div>
      
    </div>
  </div>
</div>

<div class="modal fade" id="SendNotification_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Send Notification</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
           <form method="post" action=" {{ route('userNotification') }}">
                  {{ csrf_field() }}
                  <div class="form-row">
                      <div class="col-md-12">
                       <div class="form-group">
                          <label class="small mb-1" for="inputFirstName">Product</label>
                          <input type="hidden" name="type" value="Notification">
                            <select class="form-control " name="category"  style="width: 100%;border: 1px solid #c1baba;"  autofocus>
                              <option value=" ">Select Product</option>
                              <option value="All">Send All</option>
                              @if (count($products))
                                @foreach ($products as $pd)  
                                <option value="{{ $pd['product_id']}}">{{ $pd['product_name']}}</option>
                                @endforeach
                              @else
                                <option>State Not Found</option> 
                              @endif
                            </select>
                          @if ($errors->has('category'))
                          <span class="text-danger">{{ $errors->first('category') }}</span>
                          @endif
                        </div>
                    </div>
                  </div>
                  <div class="form-row">
                    <div class="col-md-12">
                       <div class="form-group">
                        <label for="exampleInputPassword1">Message(<span style="color: #db5252;font-size:15px;">Max. 100 char required</span>)</label>
                        <textarea  name="message" maxlength="100" class="form-control textareaCount" rows="6" cols="80" style="border: 1px solid #c1baba;"  ></textarea>
                          @if ($errors->has('message'))
                            <span class="text-danger">{{ $errors->first('message') }}</span>
                            @endif
                      </div>
                      <script type="text/javascript">
                          $('textarea').maxlength({
                               alwaysShow: true,
                              threshold: 10,
                              warningClass: "label label-success",
                              limitReachedClass: "label label-danger",
                              separator: ' out of ',
                              preText: 'You write ',
                              postText: ' chars.',
                              validate: true
                          });
                    </script>
                    </div>
                  </div>
                    
                  <div class="form-row">
                    <input type="submit" class="btn btn-primary btn-block" name="submit" value="Send"> 
                    
                  </div>
                  <div class="form-group mt-4 mb-0"></div>
                </form>
      </div>
      
    </div>
  </div>
</div>