  <!DOCTYPE html>
<html lang="en"> 
  <head> 
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <title>Clyfe</title>
    @notifyCss 
     <link rel="shortcut icon" href="{{ asset('img/favicon.png') }}" />
    <link rel="stylesheet" href="{{ asset('assets/vendors/mdi/css/materialdesignicons.min.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/vendors/css/vendor.bundle.base.css') }}">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css"   >
   
   
    <!-- Start datatables -->
     <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/4.5.2/css/bootstrap.css"> 
     <link rel="stylesheet" href="{{ asset('assets/dataTable/dataTables.bootstrap4.min.css') }}"> 

 <link href="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/css/bootstrap4-toggle.min.css" rel="stylesheet">


       <!-- End  datatables --> 
    <!-- <link rel="stylesheet" href=" {{ asset('assets/css/bootstrap.min.css') }}"> -->
    <link rel="stylesheet" href=" {{ asset('assets/css/style.css') }}">
    <!-- End layout styles -->
   
    <link rel="stylesheet" href="{{ asset('assets/select2/css/select2.min.css') }}">
   <link rel="stylesheet" href="{{ asset('assets/select2-bootstrap4-theme/select2-bootstrap4.min.css') }}">
  </head>
  <body>
    <div class="container-scroller">
      <!-- partial:../../partials/_navbar.html -->
      <nav class="navbar default-layout-navbar col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
        <div class="text-center navbar-brand-wrapper d-flex align-items-center justify-content-center" style="border-bottom:1px solid #eaeaea9c;">
           <a class="navbar-brand brand-logo"  href="#"><img src="{{ asset('img/clyfe.png')  }}" style="height: 40px !important;" ></a>
          <a class="navbar-brand brand-logo-mini" href="#"><img src="{{ asset('img/favicon.png') }}" alt="logo" /></a>
       

        </div>
        <div class="navbar-menu-wrapper d-flex align-items-stretch">
          <button class="navbar-toggler navbar-toggler align-self-center" type="button" data-toggle="minimize">
            <span class="mdi mdi-menu"></span>
          </button>
          <div class="search-field d-none d-md-block">
            
          </div>
          <ul class="navbar-nav navbar-nav-right">
           <?php  $count =  DB::table('tbl_notification')->whereNull('read_at')->count(); ?>
            <li class="nav-item dropdown">
               <?php  $count =  DB::table('tbl_notification')->whereNull('read_at')->count(); ?>
               <?php if($count > 0 ) {  ?>
                <div class="alert alert-warning alert-dismissible fade show" style="padding-top: 5px !important;padding-bottom: 5px !important; margin-top: 10px !important;">
                  <strong>Notification!</strong> <span style="color: black;">{{ $count }}</span> Unread Notification <a href="{{ url('users/allNotification') }}">View</a>
                  <button type="button" class="close" data-dismiss="alert" style="padding-top: 5px;">&times;</button>
               </div>
             <?php } ?>
              </li>
           <li class="nav-item dropdown">
              <a class="nav-link count-indicator dropdown-toggle" id="messageDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
                 <i class="mdi mdi-email-outline"></i>
               <span class="count-symbol bg-danger"><?php echo  $count; ?></span>
              </a>
              <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="messageDropdown">
                
                <div class="dropdown-divider"></div>
                <a href="{{ url('users/allNotification') }}" class="dropdown-item preview-item">
                   
                  <div class="preview-item-content d-flex align-items-start flex-column justify-content-center">
                    <h6 class="preview-subject ellipsis mb-1 font-weight-normal">See all notifications</h6>
                   <!--  <p class="text-gray mb-0"> 1 Minutes ago </p> -->
                  </div>
                </a>
              </li>
             
             <li class="nav-item nav-profile dropdown">
              <a class="nav-link dropdown-toggle" id="profileDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
                <!-- <div class="nav-profile-img">
                  <img src="{{ asset('assets/images/faces/face1.jpg') }}" alt="image">
                  <span class="availability-status online"></span>
                </div> -->
                <div class="nav-profile-text">
                  <p class="mb-1 text-black"><?php echo session('Customer_logged')['name'];?> </p>
                </div>
              </a>
              <div class="dropdown-menu navbar-dropdown" aria-labelledby="profileDropdown">
              <!--   <a class="dropdown-item" href="#">
                  <i class="mdi mdi-cached mr-2 text-success"></i> My Account</a>
                <div class="dropdown-divider"></div> -->
                 
                <a class="dropdown-item"href="{{ url('users/logout') }}"
                   onclick="event.preventDefault();
                                 document.getElementById('userlogout-form').submit();">
               <i class="mdi mdi-logout mr-2 text-primary"></i>  {{ __('Logout') }}
                </a>
                   <form id="userlogout-form" action="{{ url('users/logout') }}" method="POST" style="display: none;">
                    @csrf  </form>
              </div>
            </li>
          </ul>
          <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
            <span class="mdi mdi-menu"></span>
          </button>
        </div>
      </nav>
      <!-- partial -->
      <div class="container-fluid page-body-wrapper">
        <!-- partial:../../partials/_sidebar.html -->
        <nav class="sidebar sidebar-offcanvas" id="sidebar">
          <ul class="nav">
         
            <li class="nav-item">
              <a class="nav-link" href="{{ url('users/dashboard') }}">
                <span class="menu-title">Dashboard</span>
                <i class="mdi mdi-home menu-icon"></i>
              </a>
            </li>
             
          </ul>
        </nav>
        <!-- partial -->
        <div class="main-panel">
        
            @yield('content')
        </div>


        <!-- main-panel ends -->
      </div>
      <!-- page-body-wrapper ends -->
    </div>
    <!-- Modal -->
  <div class="modal fade" id="user_viewNotificationModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Notification</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <form method="post" action="{{ route('resendNotification') }}">
        {{ csrf_field() }}
          <div class="form-group">
            <label >Category:</label>
            <input type="text" class="form-control"  name="id" id="Adminid">
            <input type="text" class="form-control"  name="category" id="Adminproduct">
          </div>
          <div class="form-group">
            <label >Message:</label>
            <textarea class="form-control"  name="message" id="Adminmessage"  maxlength="100"  rows="6" cols="80"></textarea>
          </div>
          <div class="form-row">
              <input type="submit" class="btn btn-primary btn-block" name="submit" value="Resend"> 
              
            </div>
        </form>
      </div>
      
    </div>
  </div>
</div>
    <!-- Modal-->



   <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
 
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
<script src="{{ asset('assets/vendors/js/vendor.bundle.base.js') }}"></script>
<script src="{{ asset('assets/js/off-canvas.js') }}"></script>
<script src="{{ asset('assets/js/hoverable-collapse.js') }}"></script>
<script src="{{ asset('assets/js/misc.js') }}"></script>
    <!-- endinject -->
    <!-- Custom js for this page -->
<script src="{{ asset('assets/js/file-upload.js') }}"></script>
<script src="https://cdn.jsdelivr.net/gh/gitbrent/bootstrap4-toggle@3.6.1/js/bootstrap4-toggle.min.js"></script>
<script src="{{ asset('assets/select2/js/select2.full.min.js') }}"></script>
 <script src="{{ asset('assets/dataTable/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('assets/dataTable/dataTables.bootstrap4.min.js') }}"></script>
<!-- <script src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/dataTables.buttons.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.flash.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.html5.min.js"></script>
<script src="https://cdn.datatables.net/buttons/1.6.5/js/buttons.print.min.js"></script> -->

<script type="text/javascript">
   $(function () {
    //Initialize Select2 Elements
    $('.select2').select2()

    //Initialize Select2 Elements
    $('.select2bs4').select2({
      theme: 'bootstrap4'
    })
})
</script>
<script>
   $(document).ready(function(){  
 $(function () {
    $("#example1").DataTable({
      "responsive": true, "lengthChange": false, "autoWidth": false,
      "buttons": ["copy", "csv", "excel", "pdf", "print", "colvis"]
    }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
    $('#example2').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": false,
      "ordering": true,
      "info": true,
      "autoWidth": false,
      "responsive": true,
    });
  });
   });
 
</script>
<script type="text/javascript">
    function printReport()
    {
        var prtContent = document.getElementById("reportPrinting");
        var WinPrint = window.open();
        WinPrint.document.write(prtContent.innerHTML);
        WinPrint.document.close();
        WinPrint.focus();
        WinPrint.print();
      //  WinPrint.close();
    }
</script>
  <script type="text/javascript">
    $(document).ready(function() {
         $('.example').DataTable({
           dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ]
         });
    } );
    $('.exampleButtons').DataTable( {
        dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ]
  } );
</script>  
<script type="text/javascript">
  //alert($('.textareaCount').maxlength());
         $('.textareaCount').on('click',function (){
           $('.textareaCount').maxlength();
         });
    </script>
     <script>
    function sendMarkRequest($id) {

       $.ajax({
            url: "{{ route('users.markNotification') }}",
            type:"POST",
            data: {
               "_token": "{{ csrf_token() }}",
               id:$id
             },
            success:function(response){
               $("#row_"+$id).remove();
               location.reload();
            },
           }); 
    }
    function markRequestAll() {

       $.ajax({
            url: "{{ route('users.markRequestAll') }}",
            type:"POST",
            data: {
               "_token": "{{ csrf_token() }}",
               all:'all'
             },
            success:function(response){
               $("#allbody").remove();
               //$(this).parents('div.alert').remove();
            },
           }); 
    }
 // User Notification page  
 function viewNotification($id){

    $('#viewNotification_modal').modal("show");
 
    $("#Notificationid").val($id);
    $("#NotificationProduct").val($("#NotificationProduct_"+$id).data('prod'));
    $("#NotificationMessage").val($("#NotificationProduct_"+$id).data('msg'));
 } 
    </script> 
  @include('notify::messages')
        
  <x:notify-messages />
  @notifyJs
  </body>
</html>