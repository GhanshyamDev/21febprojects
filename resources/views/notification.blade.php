@extends('layouts.customer')

@section('content')
<div class="content-wrapper">
   <div class="content-header">
      <div class="container-fluid">
        <div class="row mt-3">
          <div class="col-sm-6">
            <h1 class="m-0">  Notification</h1>
          </div><!-- /.col -->

         <!--  <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v1</li>
            </ol>
          </div>  -->
        </div> 
      </div> 
    </div>


<section class="content">

 <div class="container-fluid"> 
     
    <div class="row">
        <div class="col-md-12">
            <div class="card">
              <div class="card-body">
                <table class="table table-striped   example" style="border-bottom: none;">
                <thead>
                    <tr >
                       <th>Category</th>
                        <th>Date</th>
                        <th class="text-center"> Status </th>
                     </tr>
                </thead>
                <tbody id="allbody">
                    
                  <?php $i=0; foreach ($notifications as $notification) { $i++;?>
                  <tr  id="row_{{ $notification['noti_id'] }}" style="height:40px; ">
                   
                    <?php if($notification['Send_product_user'] != 0) { ?>
                    <td> <a href="#" onclick="viewNotification('{{ $notification['noti_id'] }}')" id="NotificationProduct_{{ $notification['noti_id'] }}" data-msg="{{ $notification['message'] }}" data-prod="{{ $notification['product_name'] }}">{{ $notification['product_name'] }}</a></td>
                    <?php } else { ?>
                       <td> <a href="#" onclick="viewNotification('{{ $notification['noti_id'] }}')" id="NotificationProduct_{{ $notification['noti_id'] }}" data-msg="{{ $notification['message'] }}" data-prod="{{ $notification['product_name'] }}">For All User</a></td>
                    <?php } ?>

                  <td> {{ date('d M Y', strtotime($notification['created_at']))  }}</td>
                  <td>  <a href="#" class="float-right mark-as-read" onclick="sendMarkRequest('{{ $notification['noti_id'] }}')" data-id="">  Mark as read   </a> </td>
                  </tr>
                   <?php } ?>
                  
                </tbody>
              </table>
                        
              </div>
            </div>
        </div>
    </div>
  </div>
 </div>
</div>

@endsection
<div class="modal fade" id="viewNotification_modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Notification</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
       <form >
        {{ csrf_field() }}
          <div class="form-group">
            <label >Category:</label>
            <input type="hidden" class="form-control"  name="id" id="Notificationid">
            <input type="text" class="form-control"  name="category" id="NotificationProduct">
          </div>
          <div class="form-group">
            <label >Message:</label>
            <textarea class="form-control"  name="message" id="NotificationMessage"  maxlength="100"  rows="6" cols="80"></textarea>
          </div>
           
        </form>
      </div>
      
    </div>
  </div>
</div>
