@extends('layouts.app')

@section('content')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mt-3 ">
          <div class="col-sm-6">
            <h2 class="m-3">User</h2>
          </div><!-- /.col -->
           <!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
          
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
           <div class="col-md-12">
             <div class="card  ">
                    @if(Session::has('success'))
                <div class="alert alert-success">
                  <strong>Success!</strong> {{Session::get('success')}}
                </div>
                @elseif(Session::has('failed'))
                <div class="alert alert-success">
                  <strong>Failed!</strong> {{Session::get('failed')}}
                </div>
                @endif
                <div class="card-body">
                     
                 <form method="post" action=" {{ url('oemAdmin/userCreate') }}">
                     {{ csrf_field() }}
                     <div class="form-row">
                       <div class="col-md-6">
                            <div class="form-group">
                                <label >Username</label>
                                <input class="form-control" name="name" type="text" placeholder="Enter Name">
                                @if ($errors->has('name'))
                                  <span class="text-danger">{{ $errors->first('name') }}</span>
                                  @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label >Email</label>
                                 <input class="form-control" name="email" type="email" placeholder="Enter Email">
                                  @if ($errors->has('email'))
                                  <span class="text-danger">{{ $errors->first('email') }}</span>
                                  @endif
                            </div>
                        </div>
                        
                    </div>
                     <div class="form-row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <labelW>Phone</label>
                                 <input class="form-control @error('phone') is-invalid @enderror" maxlength="10" name="phone" type="text" placeholder="Enter Phone No">
                                  @if ($errors->has('phone'))
                                  <span class="text-danger">{{ $errors->first('phone') }}</span>
                                  @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Password</label>
                                 <input class="form-control" name="password" type="password" placeholder="Enter Password">
                                  @if ($errors->has('password'))
                                  <span class="text-danger">{{ $errors->first('password') }}</span>
                                  @endif
                            </div> 
                        </div>
                      
                    </div>
                     <div class="form-row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Category</label>
                                 <select class="form-control  @error('category') is-invalid @enderror  " name="category" onchange="getStateCity()" style="width: 100%;"  autofocus>
                                   <option value=" ">Select Category</option>
                                     @if (count($states))
                                           @foreach ($states as $state)  
                                            <option value="{{ $state['state_id']}}">{{ $state['state_name']}}</option>
                                           @endforeach
                                      @else
                                    <option>State Not Found</option> 
                                    @endif
                                    </select>
                                  @if ($errors->has('category'))
                                  <span class="text-danger">{{ $errors->first('category') }}</span>
                                  @endif
                            </div>
                        </div>
                         <div class="col-md-6">
                            <div class="form-group">
                                <label>Geography Location</label>
                                 <input class="form-control" name="geography" type="text" placeholder="Enter Your Geography Location">
                                  @if ($errors->has('geography'))
                                  <span class="text-danger">{{ $errors->first('geography') }}</span>
                                  @endif
                            </div>
                        </div>
                      </div>  
                     <div class="form-row">
                        <div class="col-md-2">
                            <input type="submit" class="btn btn-primary btn-md" name="submit" value="Create"> 
                        </div>
                    </div>
                    <div class="form-group mt-4 mb-0"></div>
                </form>
                </div>
             </div>
            </div>
          </div>

           <div class="row">
             <div class="col-md-12">
               <div class="card mt-3">
                <div id="approveMsg"></div>
                  <div class="card-body">
                      <div class="table-responsive">
                          <table  class="table table-striped table-bordered example" style="width:100%">
                          <thead style="background-color: #2c349c;color: #fff;">
                              <tr>
                                   <th>Name</th>
                                  <th>Assign Role</th>
                                  <th>Active</th>
                                  <th>Action</th>
                              </tr>
                          </thead>
                          <tbody>
                          @if (count($oemUsers))
                             @foreach ($oemUsers as $user)  
                               <tr>
                                 <td>{{ $user['name'] }}</td>
                                 <td>{{ $user['email'] }}</td>
                                   <td>
                                    @if($user['approve'] == "Pending")
                                     <input data-id="{{ $user['id'] }}" class="toggle-class ApproveUser" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive"  data-size="sm" > 
                                    @else
 <input data-id="{{ $user['id'] }}" class="toggle-class ApproveUser" type="checkbox" data-onstyle="success" data-offstyle="danger" data-toggle="toggle" data-on="Active" data-off="InActive" checked data-size="sm" > 
                                    @endif
                                  
 
                                   </td>
                                  <td> <a href="{{ url('oemAdmin/userEdit') }}/{{ Crypt::encryptString($user['id']) }}" class="btn btn-primary btn-sm"><i class="mdi mdi-grease-pencil"></i></a>   
                                    <a href="{{ url('oemAdmin/deleteUser') }}/{{ Crypt::encryptString($user['id']) }}" class="btn btn-primary btn-sm"><i class="mdi mdi-delete-forever"></i></a>            
                                         </td>
                               </tr>
                             @endforeach
                            @else
                            <tr>UserRole Not Found</tr> 
                            @endif
                         </tbody>
                          </table>

                      </div>
                  </div>
              </div>
        
          </div>
        </div>
         <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection
 