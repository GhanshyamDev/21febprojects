 @extends('layouts.app')
 

@section('content')
 <div class="content-wrapper"> 
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mt-3">
          <div class="col-sm-6">
            <h2 class="mt-2"> Report</h2>
          </div><!-- /.col -->
         <!--  <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v1</li>
            </ol>
          </div>  -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
  <section class="content">
      <div class="container-fluid">
         <div class="card">
          <div class="card-header" style="border-bottom: none !important;">
            <div><h4 class="mb-0">Report<i class="fa fa-filter cursor-pointer text-primary float-right" aria-hidden="true" role="button" data-toggle="collapse" href="#collapseExample"  aria-expanded="false" aria-controls="collapseExample" style="font-size: 25px;"></i></h4></div>

          </div>
          <div class=" collapse" id="collapseExample">
            <div class="card-body  "  >
               <div class="form-row "> 
                 <div class="col-lg-4">
                    <div class="form-group">
                      <label class="label"><span>From Date:</span></label>
                         <input type="date" class="form-control" id="fromdate" name="fromdate">
                    </div>
                 </div>
                   <div class="col-lg-4">
                    <div class="form-group">
                      <label>From To:</label>
                      <input type="date" class="form-control" id="fromto"  name="fromto">
                    </div>
                 </div>
                   <div class="col-lg-4">
                    <div class="form-group">
                      <label>Select Service:</label>
                       <select  class="custom-select"  name="id_type" >
                         <option value="All">Select Service</option>
                         <option value="AMC">Renew AMC</option>
                         <option value="EXTENDED_WARRANTY">Extend Warranty</option>
                       </select>
                    </div>
                 </div>
               </div>
                <button class="btn btn-primary text-right btn-sm" onclick="filterRequestData()">Search</button>
            </div>
          </div> 
         
         </div>
         <br/> <br/>
       <div class="card">
        <div class="card-body">
          <div class="table-responsive "  id="dataTablesfilter">
               
          </div>
           
        </div>
        <!-- /.row (main row) -->
       </div><!-- /.container-fluid -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>

    <div class="modal fade bg-fusion-900" id="getUserRequest_Modal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
  
    <div class="modal-dialog" role="document" >
        <div class="modal-content">
            <div class="modal-header" style="border-bottom: 1px solid grey; ">
                <h5 class="modal-title" >User Request :- <span id="requestType"></span></h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                  <span aria-hidden="true">&times;</span>
                </button>
              </div>
            <div class="modal-body" >
               <form style="  padding: 10px;">
                  <div class="form-group">
                    <label style="margin-bottom:4px !important;float: left !important;" >Product Name:</label>
                    <input type="hidden" class="form-control" name="id" id="RequestId"  >
                    <input type="text" class="form-control" id="RequestProduct" readonly>
                  </div>
                  <div class="form-group">
                    <label style="margin-bottom:4px !important;float: left !important;">Message:</label>
                    <textarea class="form-control" id="RequestMessage" rows="6" cols="80"></textarea>
                  </div>
                </form>
              <button type="button" class="btn btn-primary btn-sm" onclick="closeUserRequest()">Submit Request</button>
              <!-- <button type="button" class="btn btn-primary btn-sm" data-dismiss="modal">Submit Request</button> -->
             </div>
             
        </div>
    </div>
</div>

@endsection
