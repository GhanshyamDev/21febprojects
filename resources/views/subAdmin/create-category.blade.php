@extends('layouts.app')

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mt-3">
        <div class="col-sm-6">
          <h2 class="mb-3">Category</h2>
        </div><!-- /.col -->
         <!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    @if(empty($categoryEdit))  
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
       
      <div class="row">
        <div class="col-md-12">
          <div class="card  ">
            @if(Session::has('success'))
            <div class="alert alert-success">
              <strong>Success!</strong> {{Session::get('success')}}
            </div>
            @elseif(Session::has('failed'))
            <div class="alert alert-success">
              <strong>Failed!</strong> {{Session::get('failed')}}
            </div>
            @endif
            <div class="card-body">

              <form method="post" action=" {{ route('oemAdmin.createCategory') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                
                    <div class="form-group col-md-6">
                      <label >Category</label>
                      <input class="form-control" name="category" type="text" placeholder="Enter Category">
                      @if ($errors->has('category'))
                      <span class="text-danger">{{ $errors->first('category') }}</span>
                      @endif
                    </div>
                    
                   
                <div class="form-row">
                  <div class="col-md-2">
                    <input type="submit" class="btn btn-primary btn-sm ml-3" name="submit" value="Create"> 
                  </div>
                </div>
                <div class="form-group mt-4 mb-0"></div>
              </form>
            </div>
          </div>
        </div>
       </div>

        <div class="row">
         <div class="col-md-12">  
          <div class="card mt-3">
             <div class="card-body">
               
              <div class="table-responsive">
                <table class="table table-striped table-bordered example " id=" " width="100%" cellspacing="0">
                  <thead style="background-color: #2c349c;color: #fff;">
                    <tr>
                       <th>Name</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @if (count($products)) 

                    @foreach ($products as $info)  
                    <tr>
                       <td>{{ $info['product_name'] }}</td>
                       <td>
                        <a href="{{ url('oemAdmin/categoryEdit') }}/{{ Crypt::encryptString($info['product_id']) }}" class="btn btn-primary btn-xs"><i class="mdi mdi-grease-pencil"></i></a>   
                        <a href="{{ url('oemAdmin/categoryDelete') }}/{{ Crypt::encryptString($info['product_id']) }}" class="btn btn-primary btn-xs"><i class="mdi mdi-delete-forever"></i></i></a>            
                      </td>
                    </tr> 
                    @endforeach
                    @else
                    <tr> User  Not Found</tr> 
                    @endif 
                  </tbody>
                </table>

              </div>
            </div>
          </div>

        </div>
      </div>
      <!-- /.row (main row) -->
    </div><!-- /.container-fluid -->
    @else
    <div class="container-fluid">
      <div class="row">
        <div class="col-md-12">
          <div class="card  ">
            @if(Session::has('success'))
            <div class="alert alert-success">
              <strong>Success!</strong> {{Session::get('success')}}
            </div>
            @elseif(Session::has('failed'))
            <div class="alert alert-success">
              <strong>Failed!</strong> {{Session::get('failed')}}
            </div>
            @endif
            <div class="card-body">

              <form method="post" action=" {{ route('oemAdmin.categoryUpdate') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label class="small mb-1" for="inputLastName">Category</label>
                      <input type="hidden"  value="{{ $getCategory[0]['product_id'] }}" name="category_id">
                      <input class="form-control" name="category" type="text" value="{{ $getCategory[0]['product_name'] }}" placeholder="Enter Category">
                    </div>
                  </div>
                  
                </div>

                <div class="form-row">
                  <div class="col-md-2">
                    <input type="submit" class="btn btn-primary btn-block" name="submit" value="Update"> 
                  </div>
                </div>
                <div class="form-group mt-4 mb-0"></div>
              </form>
            </div>
          </div>
        </div>

      </div>
    </div>
    @endif
  </section>
  <!-- /.content -->
</div>
@endsection
