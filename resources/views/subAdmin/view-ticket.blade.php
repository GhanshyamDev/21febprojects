 @extends('layouts.app')
 
@section('content')
 
 <div class="content-wrapper"> 
     <div class="content-header">
       <div class="content-header">
        <div class="container-fluid">
          <div class="row mt-3">
            <div class="col-sm-6">
              <h2 class="mb-3">Ticket Request </h2>
            
            </div><!-- /.col -->
             
          </div><!-- /.row -->
        </div><!-- /.container-fluid -->
      </div>
    </div>
    <!-- Main content -->
  <section class="content">
      <div class="container-fluid">
          <div class="row">
            <div class="col-lg-12  mt-3">
                <div class="card">
                  <div class="card-body">
                    <div class="table-responsive" > 
                          <table  class="table table-hover table-striped example"  id="renewList">
                              <thead style="background-color: #2c349c!important;color: #fff;">
                                <tr>
                                  <th>Message</th>
                                  
                               </tr>
                            </thead>
                            <tbody> 
                             @if(count($ticketDetails))
                              @foreach ($ticketDetails as $view)    
                              <tr onclick="replyTicket('<?php echo $view['ticket_id']; ?>','<?php echo $view['service_type']; ?>')" id="row_<?php echo $view['ticket_id']; ?>" data-mesg="<?php echo $view['description']; ?>" >
                                <td> <div class="clearfix mt-2" style="cursor: pointer;" >
                                      <h5 class="mr-2 mb-2">Service Type - {{ $view['service_type'] }}. </h5>
                                       <p class="mt-0 font-weight-light"><span style="font-weight: 600;">Message -</span> {{ $view['description'] }} </p>
                                   </div>
                                 </td> 
                               </tr>
                                @endforeach
                            @else
                              <tr>
                                <td> <div class="clearfix mt-2" style="cursor: pointer;" >
                                    <h5 class="mr-2 mb-2">Ticket Request Not Available. </h5>
                                  </div>
                               </td>
                             </tr>
                             @endif
                            </tbody>
                           
                          </table>
                      </div>
                  </div>
                  <!-- /.row (main row) -->
                 </div>
                 </div>
              </div>    
         </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
 

  <div class="modal fade"  id="replyTicketModal" tabindex="-1" role="dialog" aria-labelledby="issue" aria-hidden="true">
   
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header btn-primary text-left" style="border: 1px solid #2c349c;border: #2c349c;margin: -0.6px;margin-right: -1px;">
         <h4 class="modal-title" >Ticket Request </h4>
          <button type="button" class="close" data-dismiss="modal" aria-hidden="true" style="color: #fff;    margin: -15px -20px -25px auto !important;">×</button>
        </div>
          <form method="post" action="{{ route('replyTicket') }}">
                  {{ csrf_field() }} 
          <div class="modal-body">
            <div class="row">
             
              <div class="col-md-12">
                <p><strong>Service Type :- </strong> <span id="tickettype"></span> </p>
                <p id="ticketMSG"></p>
              </div>
            </div>
            <hr/>
            <div class="row ">
             <div class="col-md-12">
               <div class="form-group">
                    <label for="exampleInputPassword1">Reply </label>
                    <input type="hidden" name="ticketID" id="ticketID">
                    <textarea  name="reply" maxlength="100" class="form-control textareaCount" rows="6" cols="80" style="border: 1px solid #c1baba;"  ></textarea>
                      @if ($errors->has('message'))
                        <span class="text-danger">{{ $errors->first('message') }}</span>
                        @endif
                  </div>
                  <script type="text/javascript">
                      $('textarea').maxlength({
                           alwaysShow: true,
                          threshold: 10,
                          warningClass: "label label-success",
                          limitReachedClass: "label label-danger",
                          separator: ' out of ',
                          preText: 'You write ',
                          postText: ' chars.',
                          validate: true
                      });
                </script>
               </div>
            </div>
          </div>
          <div class="modal-footer">
            <button type="button" class="btn btn-danger btn-sm" data-dismiss="modal"> Cancel</button>
            <input type="submit" class="btn btn-primary btn-sm" name="submit" value="Send">
          </div>
        </form>
      </div>
    </div>
  
</div>
@endsection

 