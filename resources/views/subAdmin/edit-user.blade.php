@extends('layouts.app')

@section('content')
 <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h2 class="m-0">Edit User</h2>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v1</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->
          
    <!-- Main content -->
    <section class="content">
      <div class="container-fluid">
        <!-- Small boxes (Stat box) -->
        <div class="row">
           <div class="col-md-12">
             <div class="card  ">
                 @if(Session::has('success'))
                <div class="alert alert-success">
                  <strong>Success!</strong> {{Session::get('success')}}
                </div>
                @elseif(Session::has('failed'))
                <div class="alert alert-success">
                  <strong>Failed!</strong> {{Session::get('failed')}}
                </div>
                @endif
                <div class="card-body">
                   
                 <form method="post" action=" {{ url('oemAdmin/updateUser') }}">
                     {{ csrf_field() }}
                    <!-- <div class="form-row">
                        <div class="col-md-6">
                            <div class="form-group">
                            <label class="small mb-1" for="inputFirstName">Select Role</label>
                               <select class="form-control" name="roleID">
                               <option value=" ">Select New User Role</option>
                               @if (count($roles))
                                 @foreach ($roles as $role)  
                                   @if( $role['id'] == $asigne[0]['role_id'])
                                   <option value="{{ $role['id']}}" selected>{{ $role['name']}}</option>
                                   @else
                                    <option value="{{ $role['id']}}">{{ $role['name']}}</option>
                                    @endif
                                 @endforeach
                                @else
                                <option>UserRole Not Found</option> 
                                @endif
                                </select>
                                 @if ($errors->has('roleID'))
                                  <span class="text-danger">{{ $errors->first('roleID') }}</span>
                                  @endif
                             </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                            <label class="small mb-1" for="inputFirstName">Select Permission</label>
                               <select class=" select2" multiple="multiple" name="permission[]" style="width: 100%;color: black;">
                                
                               @if (count($permissions))
                                 @foreach ($permissions as $per)  
                                    @if( $per['id'] == $asigne[0]['permission_id'])
                                   <option value="{{ $per['id']}}" selected>{{ $per['name']}}</option>
                                    @else
                                      <option value="{{ $per['id']}}">{{ $per['name']}}</option>
                                    @endif
                                 @endforeach
                                @else
                                <option>UserRole Not Found</option> 
                                @endif
                                </select>
                                 @if ($errors->has('permission'))
                                  <span class="text-danger">{{ $errors->first('permission') }}</span>
                                  @endif
                             </div>
                        </div>
                    </div>
                     -->
                     <div class="form-row">
                       <div class="col-md-6">
                           <div class="form-group">
                                <label class="small mb-1" for="inputLastName">Username</label>
                                <input   name="user_id" type="hidden" value="{{ $asigne[0]['id'] }}"  >
                                <input class="form-control" name="name" type="text" value="{{ $asigne[0]['name'] }}" placeholder="Enter Name">
                                @if ($errors->has('name'))
                                  <span class="text-danger">{{ $errors->first('name') }}</span>
                                  @endif
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="small mb-1" for="inputFirstName">Email</label>
                                 <input class="form-control" name="email" type="email" value="{{ $asigne[0]['email'] }}" placeholder="Enter Email">
                                  
                            </div>
                        </div>
                        
                    </div>
                     <div class="form-row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="small mb-1" for="inputFirstName">Phone</label>
                                 <input class="form-control @error('phone') is-invalid @enderror" value="{{ $asigne[0]['phone'] }}" maxlength="10" name="phone" type="text" placeholder="Enter Phone No">
                                  
                            </div>
                        </div>
                        
                    </div>

                     <div class="form-row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="small mb-1" for="inputFirstName">Category</label>
                                 <select class="form-control  @error('category') is-invalid @enderror  " name="category" onchange="getStateCity()" style="width: 100%;"  autofocus>
                                   <option value=" ">Select Category</option>
                                     @if (count($states))
                                           @foreach ($states as $state)
                                           @if($asigne[0]['category_for'] == $state['state_id'])
                                            <option  value="{{ $state['state_id']}}" selected>{{ $state['state_name']}}</option>
                                           @else
                                              <option  value="{{ $state['state_id']}}" selected>{{ $state['state_name']}}</option>
                                           @endif
                                           @endforeach
                                      @else
                                    <option>State Not Found</option> 
                                    @endif
                                    </select>
                                 
                            </div>
                        </div>
                         <div class="col-md-6">
                            <div class="form-group">
                                <label class="small mb-1" for="inputFirstName">Geography Location</label>
                                 <input class="form-control" name="geography" type="text" value="{{ $asigne[0]['geography'] }}">
                                 
                            </div>
                        </div>
                      </div> 
                     
                    <div class="form-row">
                           <div class="form-group">
                                <label class="small mb-1" for="inputLastName">User Status</label>
                                 @if($asigne[0]['approve'] == 'Success')
                                    <span class="btn badge bg-succes">Verifyed</span>
                                 @else
                                     <span class="btn badge bg-danger">Pending</span>
                                 @endif
                            </div>
                      
                      
                     
                    </div>
                    <div class="form-row">
                        <div class="col-md-2">
                            <input type="submit" class="btn btn-primary btn-block" name="submit" value="Update"> 
                        </div>
                    </div>
                    <div class="form-group mt-4 mb-0"></div>
                </form>
                </div>
             </div>
          </div>
        </div>
         <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection
