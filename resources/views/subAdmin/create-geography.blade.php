
@extends('layouts.app')

@section('content')
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mt-3">
        <div class="col-sm-6">
          <h2 class="m-0">Geography</h2>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Dashboard </li>
          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <!-- /.content-header -->

  <!-- Main content -->
  <section class="content">
    @if(empty($geographyEdit))
    <div class="container-fluid">
      <!-- Small boxes (Stat box) -->
      <div class="row">
        <div class="col-md-12">
          <div class="card  ">
            @if(Session::has('success'))
            <div class="alert alert-success">
              <strong>Success!</strong> {{Session::get('success')}}
            </div>
            @elseif(Session::has('failed'))
            <div class="alert alert-success">
              <strong>Failed!</strong> {{Session::get('failed')}}
            </div>
            @endif
            <div class="card-body">

              <form method="post" action=" {{ route('oemAdmin.createGeography') }}" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-row">
                  <div class="col-md-6">
                    <div class="form-group">
                      <label>Name</label>
                      <input class="form-control" name="geography" type="text" placeholder="Enter Geography">
                      @if ($errors->has('geography'))
                      <span class="text-danger">{{ $errors->first('geography') }}</span>
                      @endif
                    </div>
                  </div>

                </div>

                <div class="form-row">
                  <div class="col-md-3">
                    <input type="submit" class="btn btn-primary btn-md" name="submit" value="Create"> 
                  </div>
                </div>
                <div class="form-group mt-4 mb-0"></div>
              </form>
            </div>
          </div>
         </div>
        </div>

        <div class="row">
         <div class="col-md-12">
          <div class="card mt-3">
            <div class="card-body">
              <div class="table-responsive">
                <table class="table table-striped table-bordered example" width="100%" cellspacing="0">
                  <thead style="background-color: #2c349c;color: #fff;">
                    <tr>
                      <th>Name</th>
                      <th>Action</th>
                    </tr>
                  </thead>
                  <tbody>
                    @if (count($geography)) 

                    @foreach ($geography as $info)  
                    <tr>
                      <td>{{ $info->name }}</td>

                      <td>
                        <a href="{{ url('oemAdmin/geographyEdit') }}/{{ Crypt::encryptString($info->id) }}" class="btn btn-primary btn-sm"><i class="mdi mdi-grease-pencil"></i></a>   
                        <a href="{{ url('oemAdmin/geographyDelete') }}/{{ Crypt::encryptString($info->id) }}" class="btn btn-primary btn-sm"><i class="mdi mdi-delete-forever"></i></a>            
                      </td>
                    </tr>
                    @endforeach
                    @else
                    <tr> User  Not Found</tr> 
                    @endif 
                  </tbody>
                </table>                        
              </table>

            </div>
          </div>
        </div>

      </div>
    </div>
    <!-- /.row (main row) -->
  </div><!-- /.container-fluid -->
  @else
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card  ">
          @if(Session::has('success'))
          <div class="alert alert-success">
            <strong>Success!</strong> {{Session::get('success')}}
          </div>
          @elseif(Session::has('failed'))
          <div class="alert alert-success">
            <strong>Failed!</strong> {{Session::get('failed')}}
          </div>
          @endif
          <div class="card-body">

            <form method="post" action=" {{ route('oemAdmin.geographyUpdate') }}" enctype="multipart/form-data">
              {{ csrf_field() }}
              <div class="form-row">
                <div class="col-md-6">
                  <div class="form-group">
                    <label >Name</label>
                    <input type="hidden" name="id" value="{{ $geography[0]->id}}">
                    <input class="form-control" name="geography" type="text"  value="{{ $geography[0]->name}}" placeholder="Enter Geography">
                    @if ($errors->has('geography'))
                    <span class="text-danger">{{ $errors->first('geography') }}</span>
                    @endif
                  </div>
                </div>
              </div>
              <div class="form-row">
                <div class="col-md-2">
                  <input type="submit" class="btn btn-primary btn-block" name="submit" value="Update" style="width:150px;"> 
                </div>
              </div>
              <div class="form-group mt-4 mb-0"></div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>    
  @endif  
</section>
<!-- /.content -->
</div>
@endsection
