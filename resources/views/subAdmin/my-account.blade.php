 @extends('layouts.app')
 

@section('content')
 <div class="content-wrapper"> 
    <!-- Content Header (Page header) -->
    <div class="content-header">
      <div class="container-fluid">
        <div class="row mt-3">
          <div class="col-sm-6">
          <h2 class="m-0"> Account</h2>
          </div><!-- /.col -->
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="#">Home</a></li>
              <li class="breadcrumb-item active">Dashboard v1</li>
            </ol>
          </div><!-- /.col -->
        </div><!-- /.row -->
      </div><!-- /.container-fluid -->
    </div>
    <!-- /.content-header -->

    <!-- Main content -->
  <section class="content">
      <div class="container-fluid">
        
          <div class="row ">
            <div class="col-md-12 ">
              <div class="card">
                 @if(Session::has('success'))
                      <div class="alert alert-success">
                        <strong>Success!</strong> {{Session::get('success')}}
                      </div>
                    @elseif(Session::has('failed'))
                      <div class="alert alert-success">
                        <strong>Failed!</strong> {{Session::get('failed')}}
                      </div>
                  @endif
                <div class="card-body">
                 <!--  <h4 class="card-title">Striped Table</h4> -->
                    <form method="post" action="{{ route('oemAdmin.my-account') }}">
                    @csrf
                   <div class="form-row">
                     <div class="col-lg-3">
                          <div class="form-group">
                             <a href="#" class="profile-picture"> <img src="{{ asset('img/avatar.png')}}" class="img-responsive img-circle" style="width: 50%;"> </a>
                         </div>
                      </div>
                      <div class="col-lg-9">
                        <div class="form-row">
                          <div class="col-lg-5">
                              <div class="form-group">
                                  <label class="small mb-1" for="inputLastName">Username</label>
                                  <input class="form-control" name="id" type="hidden" value="{{ auth()->user()->id }}">
                                  <input class="form-control" name="name" type="text" value="{{ auth()->user()->name }}">
                             </div>
                          </div>
                             <div class="col-lg-5">
                              <div class="form-group">
                                  <label class="small mb-1" for="inputFirstName">Email</label>
                                   <input class="form-control" name="email" type="email" value="{{ auth()->user()->email }}" >
                               </div>
                            </div>
                            <div class="col-lg-5">
                                <div class="form-group">
                                    <label class="small mb-1" for="inputFirstName">Mobile</label>
                                     <input class="form-control" name="phone" type="text" value="{{ auth()->user()->phone }}" >
                                 </div>
                            </div>
                        </div> 
                      </div>

                  </div>
                  
                   <div class="form-row ">
                     <div class="form-group"  style="float: right;">
                         <input type="submit" class="btn btn-primary btn-md float-sm-right"  name="submit" value="Update"> 
                      </div>
                  </div>
                   
                </form>
                </div>
              </div>
           </div>
          </div>
         
        <!-- /.row (main row) -->
      </div><!-- /.container-fluid -->
    </section>
    <!-- /.content -->
  </div>
@endsection
 