<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Products extends Model
{
     protected $table ='tbl_products';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['product_name', 'product_price', 'category_id', 'slug'];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
   public function sluggable()
    {
        return [
            'slug' => [
                'source' => 'product_name'
            ]
        ];
    }
     public $timestamps = true;

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
   
 
}
