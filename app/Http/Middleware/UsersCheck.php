<?php

namespace App\Http\Middleware;
use Closure;

class UsersCheck
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
         if(session()->has("is_customer")){
             return $next($request);
                
           } 

             return redirect('users/')->with('error', "Only admin can access!");
         
    
   
       
    }
}
