<?php

namespace App\Http\Controllers;

use App\Notifications\NotificationCreated;
use Illuminate\Http\Request;
use App\Notifications\NewPostNotify;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\Session;
use App\Http\Controllers\Auth;
use App\User;
use App\Category;
use App\Products;
use Illuminate\Support\Facades\DB;
class Notification extends Controller
{ 
     public function sendServiceNotification(Request $request)
      {    
 
      	 $users = User::first();
      	    $user = [
      	     'user_id' =>  Auth::user()->id,
      	     'name'   => 'Sonu Mishra',
      	     'email' =>  'ss@gmail.com',	
      	     ];
         $users->notify(new NotificationCreated($user));
          notify()->preset('user-updated');
      	     //  notify()->preset('user-updated');
           session::flash('success','Notification Send Successfully');
            return redirect()->back();
      }

 public function markNotification(Request $request)
    {
       
        $update = ['read_at' =>now(), ];
     
       $status = DB::table('tbl_notification')->where('noti_id', $request->id)->update($update);
       if($status) {
            return response()->json(['success'=> 200 ] );
         } else {
            return response()->json(['success'=> 300 ]);
         } 
       
   }

  // public function markAllNotification(Request $request) { 
     
  //     $update= ['read_at' =>now(), ];
  //     $status =   DB::table('notifications')->update($update);
       
  //      if($status) {
  //           return response()->json(['success'=> 200  );
  //        } else {
  //           return response()->json(['success'=> 300 ]);
  //        }
  // }



   public function displayNotification(){
  	      $i = 0;
          $dateS = \Carbon\Carbon::now()->startOfMonth()->subMonth(1);
           $dateE = \Carbon\Carbon::now() ;
       //  $data['products'] = Products::get();
        $row = DB::table('tbl_notification')->where("Send_product_user", 0)->get()->toArray();

          $data['notifications'] = User::select('tbl_notification.*','tbl_products.product_name','tbl_products.product_id')
          ->join('tbl_notification','tbl_notification.send_by','=','users.id')
          ->leftJoin('tbl_products','tbl_products.product_id','=','tbl_notification.Send_product_user')
          ->where('tbl_notification.created_at','>=',$dateS)
          ->where('tbl_notification.created_at','<=',$dateE) 
          ->whereNull('read_at')
          ->orderBy('tbl_products.created_at', 'desc')
          ->get()->toArray();     
       return view('notification',$data);
 
  } 

 

  public function  sendUserNotification(Request $request){
      
      if ($request->isMethod('post')) { 
          $request->validate([
              'category' => 'required',
              'message' => 'required',
          ]);
            if($request->category == "All"){
          
                   $data = [
                     'send_by' =>auth()->user()->id,
                     'type' => $request->type,
                     'Send_product_user' =>0,
                     'message' => $request->message,
                     'created_at'  =>now() ];

                }else {
                   $data = [
                    'send_by' =>auth()->user()->id,
                     'type' => $request->type, 
                     'Send_product_user' => $request->category,
                     'message' => $request->message,
                     'created_at'  =>now() ];
                }

            $users = DB::table('tbl_notification')->insert($data);
              if($users){
                  return back()->with("success", "Notification Send Successfully");
              } else{
                  return back()->with("failed", "Notification Send Failed");
              }
        
          
       } else {


        $dateS = \Carbon\Carbon::now()->startOfMonth()->subMonth(1);
        $dateE = \Carbon\Carbon::now() ;
        $data['products'] = Products::orderBy('product_name', 'asc')->get();
    
        $data['notifications'] = User::select('tbl_notification.*','tbl_products.product_name','tbl_products.product_id')
              ->join('tbl_notification','tbl_notification.send_by','=','users.id')
              ->leftJoin('tbl_products','tbl_products.product_id','=','tbl_notification.Send_product_user')
              ->where('tbl_notification.created_at','>=',$dateS)
              ->where('tbl_notification.created_at','<=',$dateE) 
              ->get()->toArray();
          return view('userNotification', $data); 
        
      }    
  }

  public function resendUserNotification(Request $request) {

            $user = [
               'send_by' =>auth()->user()->id,
               'message' => $request->message,
               'read_at'  =>NULL,
               'created_at'  =>now()
              ];
        $users = DB::table('tbl_notification')->where('noti_id',$request->id)->update($user);
          if($users){
              return back()->with("success", "Notification Resend Successfully");
          } else{
              return back()->with("failed", "Notification Resend Failed");
          }

  }
}  